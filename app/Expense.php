<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
    	'category',
    	'amount',
    	'expense_date',
    	'expense_notes',
    	'currency',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * @return mixed
     */
    public function getUserExpenceAttribute()
    {
        $user = Auth::user();

        return $user->expense->id;
    }


}
