<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Charts;
use App\Http\Requests\ExpenseRequest;
use Auth;


class ExpensesController extends Controller
{
    
//    protected $model;

    public function __construct(Expense $model)
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->all();
        $expenses = Charts::create('pie', 'highcharts')
                        ->title('My Expenses Chart')
                        ->elementLabel("Total")
                        ->labels($data->pluck('category'))
                        ->values($data->pluck('amount'))
                        ->responsive(true);

       
        return view('expenses.index', compact('expenses'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        //dd('it works');
        return view('expenses.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        $input = $request->all();
        Expense::create($input);

        return redirect('expenses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expense = $this->model->findOrFail($id);

        return view('expenses.show', compact('expense'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::findOrFail($id);

        return view('expenses.edit', compact('expense'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, $id)
    {
        $expense = Expense::findOrFail($id);
        $expense->update($request->all());

        return redirect('expenses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = $this->model->find($id);
        $expense->delete();

        return redirect('expenses');
    }
}
