<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
//use Request;
use Auth;
use Image;

class ProfilesController extends Controller
{
   	//protected $redirectTo = "/expenses";

   	public function index()
   	{
   		return view('expenses.index', array('user' => Auth::user()));
   	}

    public function show($id)
    {
    	$user = User::with('profile')->findOrFail($id);

    	return view('expenses.index', compact('user'));
    }

    public function edit()
    {
    	return view('profile.edit');
    }

	public function store()
	{
		
	}

    public function update(Request $request)
    {
    	if($request->hasFIle('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . "." . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300,300)->save( public_path( 'uploads/avatar' . $filename ));

    		$user = Auth::user();
    		$user->avatar = $filename;
    		$user->save();

    		return redirect('expenses.index'); 
    	}
    		return view('expenses.index', array('user' => Auth::user()));
    }
}
