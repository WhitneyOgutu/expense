A simple expense manager 

Credits:

Laravel 5.4
Bootstrap 4

#Simple guidelines :

for develop:
1. git clone 
2. git checkout -b develop

for use:

1. git clone
2. composer install
3. php artisan key:generate
4. php artisan migrate
5. php artisan serve


.env details:

1. DB_Database: expenses_manager
2. DB_Username: < your DB UserName >
3. DB_Password: < your Db password >
