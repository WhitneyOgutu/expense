<div class="form-group">

  {!! Form::label('category', 'Category:') !!}
  {!! Form::text('category', null , ['class' => 'form-control']) !!}

</div>

<div class="form-group">

  {!! Form::label('amount', 'Amount:') !!}
  {!! Form::text('amount', null , ['class' => 'form-control']) !!}

</div>

<div class="form-group">

  {!! Form::label('currency', 'Currency:') !!}
  {!! Form::select('currency', ['Ksh' => 'Kenyan Shilling', 'USD' => 'US Dollars', 'GBP' => 'British Pound', 'CND' => 'Canadian Dollar'], null, ['multiple' => true], ['class' => 'form-control']) !!}

 <!-- Form::select('size', ['L' => 'Large', 'S' => 'Small'], null, ['multiple' => true]); -->

</div>

<div class="form-group">

  {!! Form::label('expense_notes', 'Notes on Expense:') !!}
  {!! Form::textarea('expense_notes', null , ['class' => 'form-control']) !!}

</div>

<div class="form-group">

  {!! Form::label('expense_date', 'Date:') !!}

<!--  {!!  Form::date('published_at', \Carbon\Carbon::now()) !!} -->
   {!! Form::input('date' , 'expense_date', date('Y-m-d') , ['class' => 'form-control']) !!}

</div>



<div class="form-group">
  {!! form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}

</div>
