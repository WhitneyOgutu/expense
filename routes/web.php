<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', 'ExpensesController@index');
Route::resource('expenses', 'ExpensesController');
Route::resource('profile', 'ProfilesController');

/*Route::get('expenses', 'ExpensesController@index')->name('expenses.index');
Route::post('expenses', 'ExpensesController@update')->name('expenses.store');
Route::get('expenses/create', 'ExpensesController@create')->name('expenses.create');
Route::put('expenses/edit', 'ExpensesController@edit')->name('expenses.edit');
Route::get('expenses/show', 'ExpensesController@show')->name('expenses.show');
Route::delete('expenses/delete', 'ExpensesController@destroy')->name('expenses.delete');
*/






